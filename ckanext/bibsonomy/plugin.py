# Class BibSonomyConnectorPlugin
# author: Torbjoern Cunis
# date:   2014-06-07

import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckan.plugins.interfaces as interfaces

import libs.bibsonomy_api as bibsonomy

import re


class PackageDictWrapper:
    def __init__ ( self, pkg_dict ):
        self.pkg = pkg_dict
        self.extras = PackageDictWrapper.getExtrasDict( pkg_dict )

    def __getitem__ ( self, key ):
        if ( key in self.pkg ):
            return self.pkg[key]
        elif ( key in self.extras ):
            return self.extras[key]
        else:
            raise KeyError( key )

    def __setitem__ ( self, key, value ):
        self.pkg[key] = value

    def __contains__ ( self, key ):
        return ( key in self.pkg or key in self.extras )

    def getPackageDict ( self ):
        return self.pkg

    @staticmethod
    def getExtrasDict ( pkg_dict ):
        if ( not "extras" in pkg_dict ): return dict()

        #else
        d = dict()
        extras = pkg_dict["extras"]
        for pair in extras:
            k, v = pair[u'key'], pair[u'value']
            d[k] = v
        return d


class BibSonomyApi ( bibsonomy.BibSonomy ):
    def __init__ ( self, username, apikey ):
        bibsonomy.BibSonomy.__init__( self, username, apikey )

    def getLink ( self, publication_link ):
        type = user = hash = ''
        
        pattern_host = '(http://)?www.bibsonomy.org/'
        pattern_href = '(?i)('+pattern_host+'|/)?(bibtex|bookmark)/([12]?[0-9a-z]{32})/([0-9a-z_]+)\Z'
        pattern_link = '(?i)('+pattern_host+'|/)?publication/([12]?[0-9a-z]{32})/([0-9a-z_]+)\Z'

        m_href = re.match( pattern_href, publication_link )
        if ( m_href ):
            type = m_href.group(3)
            hash = m_href.group(4)
            user = m_href.group(5)

        else:
            m_link = re.match( pattern_link, publication_link )
            if ( m_link ):
                type = 'bibtex'
                hash = m_link.group(3)
                user = m_link.group(4)
            else:
                return None

        return self.getPost( type, hash, user )


class BibSonomyConnectorPlugin ( plugins.SingletonPlugin ):
    plugins.implements( plugins.IConfigurer )

    def __init__ ( self, name='' ):
        self.bibsonomy = BibSonomyApi( 'ckan-l3s', \
                                       '757cfade6dc202d81282a47c830daae9')

    def update_config ( self, config ):
        toolkit.add_template_directory( config, 'templates' )

##
##class BibSonomyConnectorPlugin ( AbstractBibSonomyConnectorPlugin ):
    plugins.implements( plugins.IPackageController, inherit=True )
    
    def before_view ( self, pkg_dict ):
        pkg = PackageDictWrapper(pkg_dict)
        
        pkg['bib_version'] = 'BibSonomyConnectorPlugin v0.1'

        if ( not 'publication' in pkg ):
            post = None
        else:
            post = self.bibsonomy.getLink( pkg['publication'] )


        if ( post ):
            pkg['bib_post'] = post

            #pkg['notes'] = post.__str__()

        else:
            pkg['bib_notes'] = 'No such post'

        return pkg.getPackageDict()
