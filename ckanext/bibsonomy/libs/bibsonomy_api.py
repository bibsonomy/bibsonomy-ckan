#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# v1.1
# extended by Torbjørn Cunis, BSc
#             Würzburg Department of Computer Science
#             Chair of Artificial Intelligence

import sys
import urllib2
from xml.dom.minidom import parseString

class BibSonomy:
    def __init__(self, username, apikey):
        self.baseurl = 'http://www.bibsonomy.org/api'
        self.username = username
        self.apikey = apikey

        # copied from the docs: http://docs.python.org/lib/urllib2-examples.html
        # Create an OpenerDirector with support for Basic HTTP Authentication...
        auth_handler = urllib2.HTTPBasicAuthHandler()
        auth_handler.add_password('BibSonomyWebService', self.baseurl, self.username, self.apikey)
        opener = urllib2.build_opener(auth_handler)
        # ...and install it globally so it can be used with urlopen.
        urllib2.install_opener(opener)

    def getDom(self, url):
        print 'Getting URL: ' + url
        f = urllib2.urlopen(self.baseurl + url)
        xml = f.read()
        xml = xml.decode('utf8', 'ignore')
        dom = parseString(xml.encode('utf-8', 'ignore'))
        return dom

    def getPosts(self, resourcetype, username='', start=0, end=10):
        if username == '': username = self.username
        type = 'resourcetype=' + getType(resourcetype)
        print 'Retrieving posts (' + resourcetype + ')'
        dom = self.getDom('/users/' + username + '/posts?' + type + '&start=' + str(start) + '&end=' + str(end))
        domPosts = dom.getElementsByTagName('post')
        posts = []
        for domPost in domPosts:
            posts += [ Post(domPost, resourcetype) ]
        return posts

    def getPost( self, resourcetype, hash, username='' ):
        if username == '': username = self.username
        type = 'resourcetype=' + getType(resourcetype)
        user = 'user=' + username
        resh = 'resource=' + hash
        print 'Retrieving post %s (%s)' %(hash, resourcetype)
        dom = self.getDom('/posts?' + type + '&' + user + '&' + resh )
        posts = dom.getElementsByTagName('post')
        if ( len(posts) == 0 ): return None
        elif ( len(posts) == 1 ):
            return Post(posts[0], resourcetype)
        else:
            raise PostHashUmbivalentException( resourcetype, hash, username )

    def getTags(self, username='', start=0, end=10):
        if username != '': username = 'user=' + username + '&'
        print 'Retrieving tags'
        dom = self.getDom('/tags?' + username + 'start=' + str(start) + '&end=' + str(end))
        domTags = dom.getElementsByTagName('tag')
        tags = []
        for domTag in domTags:
            tags += [ Tag(domTag) ]
        return tags

    def __str__ ( self ):
        return "This is BibSonomy REST-API Client for user %s." \
               %self.username

class Post:
    def __init__(self, xml, resourcetype):
        self.description = xml.getAttribute('description')
        self.postingdate = xml.getAttribute('postingdate')

        self.user = User(xml.getElementsByTagName('user')[0])
        self.group = Group(xml.getElementsByTagName('group')[0])

        # tags
        xmlTags = xml.getElementsByTagName('tag')
        self.tags = []
        for xmlTag in xmlTags:
            self.tags += [ Tag(xmlTag) ]

        # resource
        if resourcetype == 'bibtex':
            self.resource = BibTex(xml.getElementsByTagName('bibtex')[0])
        elif resourcetype == 'bookmark':
            self.resource = Bookmark(xml.getElementsByTagName('bookmark')[0])
        else:
            raise Exception('Type must be either "bookmark" or "bibtex"')

    def hasTag(self, tagname):
        for tag in self.tags:
            if tag.name == tagname: return True
        return False

    def __str__ ( self ):
        return u"Post %s@%s, %s, to %s; Tags: %s; Resource: %s"  \
               %( self.user.name, self.postingdate, self.group, \
                  self.description, self.tags, self.resource    )

class Resource:
    def __init__ ( self, type ):
        self.resourcetype = type

class Bookmark(Resource):
    def __init__(self, xml):
        Resource.__init__( self, 'bookmark' )
        self.title = xml.getAttribute('title')
        self.url = xml.getAttribute('url')

    def __str__ ( self ):
        return u"%s (%s)" %( self.title, self.url )


class BibTex(Resource):
    def __init__(self, xml):
        Resource.__init__( self, 'bibtex' )
        self.misc = xml.getAttribute('misc')
        self.note = xml.getAttribute('note')
        
        self.title = xml.getAttribute('title')
        self.author = xml.getAttribute('author')
        self.authors = [ a for a in self.author.split(' and ') if a ]
        self.bibkey = xml.getAttribute('bibtexKey')
        self.misc = xml.getAttribute('misc')
        self.abstract = xml.getAttribute('bibtexAbstract')
        self.entrytype = xml.getAttribute('entrytype')
        self.address = xml.getAttribute('address')
        self.editor = xml.getAttribute('editor')
        self.editors = [ e for e in self.editor.split(' and ') if e ]
        self.publisher = xml.getAttribute('publisher')
        self.month = xml.getAttribute('month')
        self.year = xml.getAttribute('year')
        self.url = xml.getAttribute('url')
        self.hash = xml.getAttribute('intrahash')
        self.href = xml.getAttribute('href')
        self.journal = xml.getAttribute('journal')
        self.annote = xml.getAttribute('annote')
        self.booktitle = xml.getAttribute('booktitle')
        self.chapter = xml.getAttribute('chapter')
        self.edition = xml.getAttribute('edition')
        self.howpublished = xml.getAttribute('howpublished')
        self.institution = xml.getAttribute('institution')
        self.organization = xml.getAttribute('organization')
        self.number = xml.getAttribute('number')
        self.pages = xml.getAttribute('pages')
        self.school = xml.getAttribute('school')
        self.series = xml.getAttribute('series')
        self.volume = xml.getAttribute('volume')
        self.day = xml.getAttribute('day')
        self.type = xml.getAttribute('type')

    def __str__ ( self ):
        return u"%s. %s" %( self.author, self.title )

class User:
    def __init__(self, xml):
        self.name = xml.getAttribute('name')

    def __str__ ( self ):
        return u"User %s" %self.name


class Group:
    def __init__(self, xml):
        self.name = xml.getAttribute('name')

class Tag:
    def __init__(self, xml):
        self.name = xml.getAttribute('name')
        if xml.getAttribute('usercount') != '':
            self.usercount = int(xml.getAttribute('usercount'))
        if xml.getAttribute('globalcount') != '':
            self.globalcount = int(xml.getAttribute('globalcount'))

#
# Helper
#
def getType(type):
    if type == 'bookmark' or type == 'bibtex':
        return type;
    raise Exception('Type must be either "bookmark" or "bibtex"')

#
# Exceptions
#
class PostHashUmbivalentException ( Exception ):
    def __init__ ( self, resourcetype, hash, user ):
        self.type = resourcetype
        self.hash = hash
        self.user = user

    def __str__ ( self ):
        return u'Hash \'%s\' is umbivalent for user %s and type %s' \
               %( self.hash, self.user, self.type )

#
# Main
#
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: ' + sys.argv[0] + ' [username]'
        sys.exit()

    username = sys.argv[1]

    bibsonomy = BibSonomy(YOUR_USERNAME, YOUR_APIKEY)
    findPrivateTags(bibsonomy, username)
