from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(
    name='ckanext-bibsonomy',
    version=version,
    description="Extension to connect CKAN to BibSonomy.",
    long_description='''
    ''',
    classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Torbjoern Cunis',
    author_email='torbjoern.cunis@stud-mail.uni-wuerzburg.de',
    url='http://www.bibsonomy.org/',
    license='',
    packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
    namespace_packages=['ckanext', 'ckanext.bibsonomy'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        # -*- Extra requirements: -*-
    ],
    entry_points='''
        [ckan.plugins]
        bibsonomy_connector=ckanext.bibsonomy.plugin:BibSonomyConnectorPlugin
        # Add plugins here, e.g.
        # myplugin=ckanext.bibsonomy.plugin:PluginClass
    ''',
)
