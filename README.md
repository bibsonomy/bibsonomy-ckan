# BibSonomy CKAN #

The BibSonomy CKAN extension connects CKAN to the BibSonomy database.


## How to install ##

Prerequisite of the BibSonomy CKAN extension is, indeed, a working source install of CKAN on your system. If you haven't installed CKAN yet you may visit [Installing CKAN from source](http://docs.ckan.org/en/ckan-2.2/install-from-source.html) first.

1. Navigate to your desired source folder to install the extension into (e.g. *ckan/default/src*);
2. create a new folder *ckanext-bibsonomy* (sic!) and clone the BibSonomy CKAN repository into;
3. activate your CKAN virtual environment and install the BibSonomy CKAN extension via *python ckanext-bibsonomy/setup.py develop*;
4. enable the BibSonomy CKAN plug-in by adding it to the list plug-ins in your CKAN config file:
    
```
#!
ckan.plugins = [...] bibsonomy_connector
```

**Run CKAN. You're done!**